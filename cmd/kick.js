module.exports = {
  command: function(message, args){
    mention = args[0];
    member = message.mentions.members.first();
    reason = args.slice(1).join(' ');
    member.kick(reason)
      .catch(() => {message.channel.send(`Could not kick user ${member}.`)});
  },
  helpJson: {
    name: "kick",
    description: "Kick user mentioned.",
    use: "[usermention] <(reason)>",
    developer: false,
    administrator: true
  }
}
