module.exports = {
  command: function(message, args){
    mention = args[0];
    member = message.mentions.members.first();
    reason = args.slice(1).join(' ');
    member.ban(reason)
      .catch(() => {message.channel.send(`Could not bad user ${member}.`)});
  },
  helpJson: {
    name: "ban",
    description: "Ban user mentioned.",
    use: "[usermention] <(reason)>",
    developer: false,
    administrator: true
  }
}
