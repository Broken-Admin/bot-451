module.exports = {
  command: function (message, args, fourfiveone) {
    cmd = args.join(" ");
    try {
      eval(cmd);
    } catch (e) {
      message.channel.send('Error! See console.');
      console.log(e);
    }
  },
  helpJson: {
    name: "runjs",
    description: "Run provided command(s)",
    use: "<[command(s)]>",
    developer: true,
    administrator: false
  }
}