const config = require('../config.json');
const Discord = require('discord.js');
const Delay = require('delay')
const _ = require('underscore');
const quotes = require('../misc/451-quotes.json');
module.exports = {
  command: async function (Commands, args, msg, member) {
    let cmdHlp = []; // Stores the helpJson of all commands loaded.
    for (let i = 0; i < Commands[0].length; i++) {
      cmdHlp.push(Commands[0][i].helpJson);
    }
    if (args[0].length >= 1 && args[0].length != 0) {
      if(Commands[1][args[0]].developer && !member[1] || Commands[1][args[0]].administrator && !member[0]) return;
      if (Commands[1][args[0]]) { 
        msg.channel.send(`\`${config.prefix}${Commands[1][args[0]].helpJson.name} ${Commands[1][args[0]].helpJson.use}\``);
        return;
      } else {
        msg.channel.send('Could not find command.').then(async function (couldNot) {
          await Delay(2000);
          couldNot.delete();
        });
        await Delay(2000);
      }
    }
    let hlpEmbed = new Discord.RichEmbed()
      .setTitle("451 Help Message")
      .setAuthor("451", "https://i.imgur.com/EQOVe6k.jpg")
      .setColor(_.sample(config.generalcolors.primary))
      .setFooter(`\"${_.sample(quotes)}\" ― Ray Bradbury, Fahrenheit 451`)
      .setTimestamp();
    for (let x = 0; x < cmdHlp.length; x++) {
      let addField = true;
      if ((!member[0] && cmdHlp[x].administrator) || (!member[1] && cmdHlp[x].developer)) {
        addField = false;
      }
      if (addField) {
        if (cmdHlp[x].administrator && cmdHlp[x].developer) {
          hlpEmbed.addField(`${config.prefix}${cmdHlp[x].name} | Admin & Dev Command`, `${cmdHlp[x].description}`);
        } else {
          if (cmdHlp[x].administrator) {
            hlpEmbed.addField(`${config.prefix}${cmdHlp[x].name} | Administrator Command`, `${cmdHlp[x].description}`);
          } else if (cmdHlp[x].developer) {
            hlpEmbed.addField(`${config.prefix}${cmdHlp[x].name} | Developer Command`, `${cmdHlp[x].description}`);
          } else {
            hlpEmbed.addField(`${config.prefix}${cmdHlp[x].name}`, `${cmdHlp[x].description}`);
          }
        }
      }
    }
    msg.channel.send(hlpEmbed).then(m => {
      m.react('❌');
    })
  },
  helpJson: {
    name: "help",
    description: "View all commands.",
    use: "(command)",
    developer: false,
    administrator: false
  }
}
