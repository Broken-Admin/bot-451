module.exports = {
  command: function (message, args) {
    args = args.join(' ');
    if(args.length <= 0) {
      message.channel.send(`You must provide a message to send, ${message.author}`);
    } else {
      message.channel.send(args).then((msg) => {
        message.delete();
      })
    }
  },
  helpJson: {
    name: "print",
    description: "Send message to channel.",
    use: "<[message]>",
    developer: true,
    administrator: false
  }
}
