module.exports = {
  command: function (message, args) {
    message.channel.send('<a:loading:457436395875729418>Restarting<a:loading:457436395875729418>').then(() => {
      console.log('Exiting under \"Restart\" command.');
      process.exit();
    });
  },
  helpJson: {
    name: "restart",
    description: "Restart the bot.",
    use: "",
    developer: true,
    administrator: false
  }
}
