const fs = require('fs');
const config = require('../config.json');
module.exports = {
  // Loads command file exports into an object, referenced by their helpJson name
  loadCommandToObject: function (dir) {
    let commands = [];
    fs.readdir(dir.toString(), (err, files) => {
      for (let i = 0; i < files.length; i++) {
        if (files[i].match(/.js$/g) && files[i] != 'template.js') {
          let cmd = require(`../${dir}/${files[i]}`);
          commands[cmd.helpJson.name] = cmd;
        }
      }
    });
    return (commands);
  },
  // Loads command file exports into an unsorted array
  loadCommandsToArray: function (dir) {
    let commands = [];
    fs.readdir(dir.toString(), (err, files) => {
      for (let i = 0; i < files.length; i++) {
        if (files[i].match(/.js$/g) && files[i] != 'template.js') {
          let cmd = require(`../${dir}/${files[i]}`);
          commands.push(cmd);
        }
      }
    });
    return (commands);
  },
  // Handles all incoming messages
  handleMessage: function (message, commands, commandsArr, fourfiveone) {
    if (!message.content.startsWith(config.prefix)) return; // If content doesn't start with prefix, return
    // Get the command name by removing prefix and arguments
    let command = message.content.slice(config.prefix.length).trim().split(/ +/g)[0].replace(config.prefix, '')
    if (!commands[command]) return; // If command does not exist, return
    // If the command is a developer or administrator command and the user is not the developer or administrator, return
    if ((commands[command].developer && message.member.id != config.developer) || (commands[command].administrator && message.member.hasPermission('ADMINISTRATOR'))) return;
    // Get provided arguments from message content
    let args = message.content.slice(config.prefix.length + command.length).trim().split(/ +/g);
    if (command == 'help') { // If command is the help command, provide custom arguments
      // memberStatus[0], bool, if user is admin
      // memberStatus[1], bool, if user is developer
      let memberStatus = [message.member.hasPermission('ADMINISTRATOR') ? true : false, (message.member.id == config.developer) ? true : false]
      // Help message args - (commands[], args, msg, member)
      commands[command].command([commandsArr, commands], args, message, memberStatus);
      return;
    } else if(command == 'runjs') { // If command is runjs command, provide custom arguments
      // Runjs args - (message, args, fourfiveone)
      commands[command].command(message, args, fourfiveone)
    } else {
      let runCmd = true;
      if (commands[command].helpJson.administrator && !message.member.hasPermission('ADMINISTRATOR') || commands[command].helpJson.developer && message.member.id != config.developer) runCmd = false;
      if (runCmd) {
        // Other command args - (message, args)
        commands[command].command(message, args);
      }
    }
  }
}