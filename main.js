// Config
const config = require('./config.json');
// Discord
const Discord = require('discord.js');
const Embed = Discord.RichEmbed;
// Client
const fourfiveone = new Discord.Client();
// Commands Handler
const handler = require('./handler/handler.js');
// Commands
const Commands = handler.loadCommandToObject('cmd');
const CommandsArr = handler.loadCommandsToArray('cmd');

fourfiveone.on('ready', () => {
  console.log(`Logged in!\n${new Date()}`);
  fourfiveone.user.setPresence({
    status: 'online',
    game: { // "Watching a book burning! (${prefix})"
      name: `a book burning! (${config.prefix}help)`,
      type: 3
    }
  });
});

fourfiveone.on('messageReactionAdd', (reaction, user) => {
  if(reaction.emoji.name == '❌' && !user.bot && reaction.message.author.bot){
    reaction.message.delete();
  }
})

fourfiveone.on('message', message => handler.handleMessage(message, Commands, CommandsArr, fourfiveone));

fourfiveone.login(config.token);